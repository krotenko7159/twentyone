from random import *
import time
# _________Players params

playerNumber = 0
playerScore = 0
dealerNumber = 0
dealerScore = 0
secPlayerNumber = 0
secPlayerScore = 0

# _________Players Names

playerName = ""
secPlayerName = ""

# _________Answers params

answer = "y"
dealerAnswer = "y"
playersCount = ""

print("Card Game 21! Try To Play!")

print("With whom you wanna play? PvP (Player vs Player), PvD (Player vs Dealer) or 3 players(Two players and Dealer)?")
playersCount = input()
playersCount = playersCount.lower()

# _______________PVP (2 PLAYERS)

if playersCount == "pvp":
    playerAnswer = "y"
    secPlayerAnswer = "y"

    # ____________Player Names

    print("Enter the name of the FIRST player: ")
    playerName = input()
    print("Enter the name of the SECOND player ")
    secPlayerName = input()

    while playerAnswer == "y" or secPlayerAnswer == "y":

        # __________First Player Params
        if playerAnswer == "y":
            playerNumber = randrange(1, 11)
            playerScore += playerNumber
            if playerScore > 21:
                print(playerName, ",you've got ", playerNumber)
                print(playerName, "Too much, you lose. Your score = ", playerScore, " points\n")
                playerAnswer = "n"
            elif playerScore < 21:
                print(playerName, "You've got ", playerNumber)
                print(playerName, "Your score ", playerScore, "\n")
                print(playerName, "One more card? (Y / N) \n")

                # ___________Answer Mechanics FIRST PLAYER

                playerAnswer = input()
                playerAnswer = playerAnswer.lower()
                if playerAnswer == "stop":
                    continue
                elif playerAnswer == "y":
                    continue
                elif playerAnswer == "n":
                    print(playerName, "Your score = ", playerScore, " points")
                    if playerScore >= 18:
                        print(playerName, "Oh, you missed quite a bit")
                    elif playerScore < 18:
                        print(playerName, "You gave up completely in vain, there were chances")
                    elif playerScore < 10:
                        print(playerName, "Did you change your mind about playing, or what?")
                    playerAnswer = "stop"
                else:
                    print(playerName, " Enter correct answer, Y/N")
                    playerAnswer = input()
                    playerAnswer = playerAnswer.lower()
            elif playerScore == 21:
                print("You've got ", playerNumber)
                print("Wow! It's 21. ", playerName, " You win this time =) ")
                playerAnswer = "stop"
        # __________SECOND Player Params
        if secPlayerAnswer == "y":
            secPlayerNumber = randrange(1, 11)
            secPlayerScore += secPlayerNumber

        if secPlayerScore > 21:
            print(secPlayerName, ",you've got ", secPlayerNumber)
            print(secPlayerName, " Too much, you lose. Your score = ", secPlayerScore, " points\n")
            secPlayerAnswer = "n"
        elif secPlayerScore < 21:
            print(secPlayerName, " You've got ", secPlayerNumber)
            print(secPlayerName, " Your score ", secPlayerScore, "\n")
            print(secPlayerName, " One more card? (Y / N) \n")
            # ___________Answer Mechanics SECOND PLAYER
            secPlayerAnswer = input()
            secPlayerAnswer = secPlayerAnswer.lower()
            if secPlayerAnswer == "stop":
                continue
            if secPlayerAnswer == "y":
                continue
            elif secPlayerAnswer == "n":
                print(secPlayerName, " Your score = ", secPlayerScore, " points")
                if secPlayerScore >= 18:
                    print(secPlayerName, " Oh, you missed quite a bit")
                elif secPlayerScore < 18:
                    print(secPlayerName, " You gave up completely in vain, there were chances")
                elif secPlayerScore < 10:
                    print(secPlayerName, " Did you change your mind about playing, or what?")
                secPlayerAnswer = "stop"
            else:
                print("Enter correct answer, Y/N")
        elif secPlayerScore == 21:
            print("You've got ", secPlayerNumber)
            print("Wow! It's 21. ", secPlayerName, " You win this time =) ")
            secPlayerAnswer = "stop"
        if playerAnswer == "stop" and secPlayerAnswer == "stop":
            break

    print(" Well played, friends! \n", playerName, "'s score is", playerScore, "points.\n", secPlayerName,
          "'s score is ", secPlayerScore)
    if secPlayerScore > 21 and secPlayerScore < playerScore:
        print("Hey, ", secPlayerName, " seems like you're winner! GRATZ")
    elif secPlayerScore <= 21 and secPlayerScore > playerScore:
        print("Hey, ", secPlayerName, " seems like you're winner! GRATZ")
    elif secPlayerScore == 21 and playerScore == 21:
        print("It's a DRAW, guys. Incredible!")
    elif secPlayerScore == playerScore:
        print("It's a DRAW, guys. Incredible!")
    elif secPlayerScore < 21 and playerScore > 21:
        print("Hey, ", secPlayerName, " seems like you're winner! GRATZ")
    else:
        print("Hey, ", playerName, " seems like you're winner! GRATZ")


# ______________PvD (2 PLAYERS)

def dealers_ai():
    if dealerScore <= 19:
        dealerAnswer = "y"
        return dealerAnswer
    elif dealerScore == 21:
        dealerAnswer = "stop"
        return dealerAnswer
    elif dealerScore > 21:
        dealerAnswer = "stop"
        return dealerAnswer


if playersCount == "pvd":
    playerAnswer = "y"
    dealerAnswer = "y"

    # ____________Player Names

    print("Enter the name of the player: ")
    playerName = input()

    while playerAnswer == "y" or dealerAnswer == "y":

        # __________First Player Params
        if playerAnswer == "y":
            playerNumber = randrange(1, 11)
            playerScore += playerNumber
            if playerScore > 21:
                print(playerName, ",you've got ", playerNumber)
                print(playerName, "Too much, you lose. Your score = ", playerScore, " points\n")
                playerAnswer = "n"
            elif playerScore < 21:
                print(playerName, "You've got ", playerNumber)
                print(playerName, "Your score ", playerScore, "\n")
                print(playerName, "One more card? (Y / N) \n")

                # ___________Answer Mechanics FIRST PLAYER

                playerAnswer = input()
                playerAnswer = playerAnswer.lower()
                if playerAnswer == "stop":
                    continue
                elif playerAnswer == "y":
                    continue
                elif playerAnswer == "n":
                    print(playerName, "Your score = ", playerScore, " points")
                    if playerScore >= 18:
                        print(playerName, "Oh, you missed quite a bit")
                    elif playerScore < 18:
                        print(playerName, "You gave up completely in vain, there were chances")
                    elif playerScore < 10:
                        print(playerName, "Did you change your mind about playing, or what?")
                    playerAnswer = "stop"
                else:
                    print(playerName, " Enter correct answer, Y/N")
                    playerAnswer = input()
                    playerAnswer = playerAnswer.lower()
            elif playerScore == 21:
                print("You've got ", playerNumber)
                print("Wow! It's 21. ", playerName, " You win this time =) ")
                playerAnswer = "stop"
        # __________DEALER Params
        if dealerAnswer == "y":
            dealerNumber = randrange(1, 11)
            dealerScore += dealerNumber

        if dealerScore > 21:
            print("Dealer got ", dealerNumber)
            print("Dealer lose, he took to much. Dealers score = ", dealerScore, " points\n")
            time.sleep(2) # NEW
            dealerAnswer = "n"
        elif dealerScore < 21:
            print("Dealer got ", dealerNumber)
            print("Dealer score ", dealerScore, "\n")
            
            # ___________Answer Mechanics DEALER
            dealers_ai()
            if dealerAnswer == "stop":
                continue
            if dealerAnswer == "y":
                time.sleep(2)
                continue
            elif dealerAnswer == "n":
                print("Dealer's score = ", dealerScore, " points")
                dealerAnswer = "stop"
        elif dealerScore == 21:
            print("Dealer got ", dealerNumber)
            print("It's 21 for a dealer. He wins this time :(")
            dealerAnswer = "stop"
        if playerAnswer == "stop" and dealerAnswer == "stop":
            break

    print(" Well played, friends! \n", playerName, "'s score is", playerScore, "points.\n", 
          "Dealer's score is ", dealerScore)
    if dealerScore > 21 and dealerScore < playerScore:
        print("Hey, DEALER wins.")
    elif dealerScore <= 21 and dealerScore > playerScore:
        print("Hey, DEALER wins.")
    elif dealerScore == 21 and playerScore == 21:
        print("It's a DRAW, guys. Incredible!")
    elif dealerScore == playerScore:
        print("It's a DRAW, guys. Incredible!")
    elif dealerScore < 21 and playerScore > 21:
        print("Hey, DEALER wins.")
    else:
        print("Hey, ", playerName, " seems like you're winner! GRATZ")
