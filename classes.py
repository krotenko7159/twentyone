from random import *


class Player:

    def __init__(self):
        self.name = input("Enter your name: ")
        self.answer = "y"
        self.score = 0
        self.number = 0
        self.winner = 0

    def ask(self):
        if self.answer == "y":
            self.answer = input("One more card? \n")
            self.answer = self.answer.lower()

    def show_result(self):
        print(self.name, ",you've got ", self.number)
        print(self.name, " Your score ", self.score, "\n")
        if self.score > 21:
            print(self.name, "Too much, you lose. Your score = ", self.score, " points\n")
            self.answer = "n"
        elif self.score < 21:
            Player.ask(self)
        elif self.score == 21:
            print("Hey, ", self.name, "seems like you are winner! You've take 21")
            self.answer = "n"
        elif self.answer == "n":
            print("Hey, ", self.name, "your finally score is ", self.score)

    def show_dealer_result(self):
        print(self.name, ",you've got ", self.number)
        print(self.name, " Your score ", self.score, "")
        if self.score > 21:
            print(self.name, "Too much, you lose. Your score = ", self.score, " points\n")
            self.answer = "n"
        elif self.score < 21:
            self.answer = "y"
        elif self.score == 21:
            print("Hey, ", self.name, ", seems like you are winner! You've take 21")
            self.answer = "n"
        if self.answer == "n":
            print("Hey, ", self.name, ", your finally score is ", self.score)

    def take_card(self):
        if self.answer == "y":
            self.number = randrange(1, 11)
            self.score += self.number
            return self.score

    def congratulations(self):
        print(self.name, ", you're a winner!")

    def __eq__(self, other):
        if self.answer == "n" and other.answer == "n":
            if self.score == other.score and self.score == 21 and other.score == 21:
                print("Hey, guys, it's a draw")
            elif 21 > self.score > other.score:
                self.winner = True
            elif self.score == 21 and other.score < 21:
                other.winner = True
            else:
                other.winner = True

    def who_winner(self, other):
        if self.answer == "n" and other.answer == "n":
            if self.score == other.score and self.score == 21 and other.score == 21:
                print("Hey, guys, it's a draw")
            elif 21 > self.score > other.score:
                self.winner = True
            elif self.score == 21 and other.score < 21:
                self.winner = True
            elif self.score < 21 and other.score > 21:
                self.winner = True
            else:
                other.winner = True

    def gameplay(self):
        self.take_card()
        self.show_result()

    def gameplay_dealer(self):
        if self.score <= 19:
            self.answer = "y"
        elif self.score == 21:
            self.answer = "n"
        elif self.score > 21:
            self.answer = "n"
        elif self.score == 20:
            self.answer = "n"
        self.take_card()
        self.show_dealer_result()

