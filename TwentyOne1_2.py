from classes import Player

gamemode = input("Card Game 21! \n Choose a Game Mode: PvP (Player VS Player), PvD (Player VS Dealer) or 3 players?")

if gamemode == "pvp":
    player1 = Player()
    player2 = Player()
    while player1.answer == "y" or player2.answer == "y":
        player1.gameplay()
        player2.gameplay()

    if player1.winner:
        player1.congratilations()
    elif player2.winner:
        player2.congratilations()

if gamemode == "pvd":
    player1 = Player()
    dealer = Player()
    dealer.name = "Dealer"
    while player1.answer == "y" or dealer.answer == "y":
        player1.gameplay()
        dealer.gameplay_dealer()
        player1.who_winner(dealer)

    if player1.winner:
        player1.congratulations()
    elif dealer.winner:
        dealer.congratulations()

if gamemode == "3":
    player1 = Player()
    player2 = Player()
    dealer = Player()
    dealer.name = "Dealer"
    while player1.answer == "y" or player2.answer == "y" or dealer.answer == "y":
        player1.gameplay()
        player2.gameplay()
        dealer.gameplay_dealer()
        player1.who_winner(player2)
        player2.who_winner(dealer)

    if player1.winner:
        player1.congratulations()
    elif player2.winner:
        player2.congratulations()
    elif dealer.winner:
        dealer.congratulations()